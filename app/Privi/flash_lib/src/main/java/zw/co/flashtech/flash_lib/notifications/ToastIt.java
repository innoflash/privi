package zw.co.flashtech.flash_lib.notifications;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by streamer on 9/12/2016.
 */
public class ToastIt {
    public static void message(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
