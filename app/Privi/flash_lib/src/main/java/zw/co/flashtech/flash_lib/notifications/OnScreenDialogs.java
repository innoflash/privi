package zw.co.flashtech.flash_lib.notifications;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by john on 2018/01/18.
 */

public class OnScreenDialogs {
    private static ProgressDialog pDialog;

    public static ProgressDialog getpDialog() {
        return pDialog;
    }

    /**
     * <b>public static void showAsync(Context context, String message)</b>
     * This method shows a background process happening on the User Interface in a circular motion
     *
     * @param context The context usually an activity where the background process has to happen
     * @param message The message ypu want to show the user while the user is waiting for the background process to finish
     */
    public static void showAsync(Context context, String message) {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage(message);
        pDialog.setIndeterminate(false);
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(true);
        pDialog.show();
    }

    /**
     * <b>public static void showLinearAsync(Context context, String message)</b>
     * This method shows a background process happening on the User Interface in a linear motion
     *
     * @param context The context usually an activity where the background process has to happen
     * @param message The message ypu want to show the user while the user is waiting for the background process to finish
     */
    public static void showLinearAsync(Context context, String message) {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage(message);
        pDialog.setIndeterminate(true);
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pDialog.setCancelable(true);
        pDialog.show();
    }

    /**
     * This dismisses the progressbar off the user`s screen
     */
    public static void dismissAsync() {
        pDialog.dismiss();
    }

}
