package zw.co.flashtech.flash_lib.recyclerview;

import android.view.View;

/**
 * Created by drowndeep on 3/21/2016.
 */
public interface RecyclerViewClickListener {
    void onClick(View view, int position);
    void onLongClick(View view, int position);
}
