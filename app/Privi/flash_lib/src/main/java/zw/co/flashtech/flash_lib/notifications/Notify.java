package zw.co.flashtech.flash_lib.notifications;

import android.app.PendingIntent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.TaskStackBuilder;

import zw.co.flashtech.flash_lib.R;

/**
 * Created by streamer on 3/31/2017.
 */

public class Notify {
    public static void notify(Notification notification) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(notification.getContext());
        builder.setSmallIcon(notification.getIcon() == 0 ? R.drawable.flashtech : notification.getIcon());
        builder.setContentTitle(notification.getTitle() == null ? "no title" : notification.getTitle());
        builder.setContentText(notification.getContentText() == null ? "no text" : notification.getContentText());
        builder.setDefaults(android.app.Notification.DEFAULT_SOUND);
        builder.setAutoCancel(true);
        builder.setTicker(notification.getTitle() == null ? "no title" : notification.getTitle());

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(notification.getContext());
        stackBuilder.addParentStack(notification.getMyClass());
        stackBuilder.addNextIntent(notification.getIntent());

        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(resultPendingIntent);

        android.app.Notification notifications = builder.build();
        NotificationManagerCompat.from(notification.getContext()).notify(notification.getNoficationID(), notifications);
    }
}
