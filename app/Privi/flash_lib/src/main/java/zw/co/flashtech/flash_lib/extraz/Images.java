package zw.co.flashtech.flash_lib.extraz;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.FileInputStream;

/**
 * Created by john on 2016/12/29.
 */

public class Images {
    /**
     * This method returns bitmaps of images stored in the internal memory of android
     * @param fis The file input stream of the image to be shown
     * @return Returns the image in bitmap format
     */
    public static Bitmap decodeThumb(FileInputStream fis) {
        try {
            //decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            //o.inJustDecodeBounds = true;
            Bitmap bitmap = BitmapFactory.decodeStream(fis, null, o);
            return bitmap;
        } catch (Exception e) {
            return null;
        }
    }
}
