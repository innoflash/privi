package zw.co.flashtech.flash_lib.internet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import zw.co.flashtech.flash_lib.extraz.Utils;

/**
 * Created by john on 2017/01/06.
 */

public class ThumbDownload {
    public static boolean downloadThumb(String url, FileOutputStream oss) {
        boolean results;

        URL imageUrl = null;
        try {
            imageUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is = conn.getInputStream();
            FileOutputStream os = oss;
            Utils.CopyStream(is, os);
            os.close();
            results = true;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            results = false;
            //Message.message(context, "user does not have photo");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            results = false;
        } catch (IOException e) {
            e.printStackTrace();
            results = false;
        }
        return results;
    }

    public static boolean downloadFile(String url, String filename, File folder) {
        boolean results;
        File savedImage = new File(folder, filename);

        URL imageUrl = null;
        try {
            imageUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is = conn.getInputStream();
            OutputStream os = new FileOutputStream(savedImage);
            Utils.CopyStream(is, os);
            os.close();
            results = true;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            results = false;
            //Message.message(context, "user does not have photo");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            results = false;
        } catch (IOException e) {
            e.printStackTrace();
            results = false;
        }
        return results;
    }

}
