package zw.co.flashtech.flash_lib.notifications;

import android.content.Context;
import android.content.Intent;

/**
 * Created by streamer on 3/31/2017.
 */

public class Notification {
    private Context context;
    private String title;
    private String contentText;
    private Intent intent;
    private Class myClass;
    private int noficationID;
    private int icon;

    public Notification(Context context, int noficationID) {
        this.context = context;
        this.noficationID = noficationID;
    }

    public Context getContext() {
        return context;
    }

    public int getNoficationID() {
        return noficationID;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContentText() {
        return contentText;
    }

    public void setContentText(String contentText) {
        this.contentText = contentText;
    }

    public Intent getIntent() {
        return intent;
    }

    public void setIntent(Intent intent) {
        this.intent = intent;
    }

    public Class getMyClass() {
        return myClass;
    }

    public void setMyClass(Class myClass) {
        this.myClass = myClass;
    }
}
