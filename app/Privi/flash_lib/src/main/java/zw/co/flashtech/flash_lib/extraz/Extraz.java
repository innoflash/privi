package zw.co.flashtech.flash_lib.extraz;

import android.content.Context;

import java.io.File;
import java.util.Random;

/**
 * Created by john on 2017/01/04.
 */

public class Extraz {
    /**
     * This calculates a random number from zero upto the maximum number
     * @param max_num This is the maximum number a random number can be
     * @return The random number generated here
     */
    public static int randInt(int max_num) {
        Random rand = new Random();
        return rand.nextInt(max_num - 1);
    }

    /**
     * This finds any file stored in the internal memory of android
     * @param context The context in which the file is to be used
     * @param file_name The name of the file we are looking for
     * @return The file or null if it does not exist
     */
    public static File getInternalFile(Context context, String file_name) {
        return new File(context.getFilesDir(), file_name);
    }
}
