package zw.co.flashtech.flash_lib.preferences;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by streamer on 9/12/2016.
 */
public class Prefs {
    private Context context;
    private String prefFileName;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;

    /**
     * This initialises your preference manager
     * @param context this is the context you want to use the preference from
     * @param prefFileName this is the name of the preference managung xml file
     */
    public Prefs(Context context, String prefFileName) {
        this.context = context;
        this.prefFileName = prefFileName;
    }

    /**
     * This gets the context used when initialising the preference
     * @return The context used
     */
    public Context getContext() {
        return context;
    }


    /**
     * Loads the name of the preferences xml file
     * @return The name of the preferences xml file
     */
    public String getPrefFileName() {
        return prefFileName;
    }

    /**
     * Saves boolean value to the preferences
     * @param pref_name This is the name used to map to a boolean value
     * @param pref_value This be the boolean value to be saved
     */
    public void saveBoolean(String pref_name, boolean pref_value) {
        sp = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
        editor = sp.edit();
        editor.putBoolean(pref_name, pref_value);
        editor.apply();
    }


    /**
     * Saves integer value to the preferences
     * @param pref_name This is the name used to map to an integer value
     * @param pref_value This be the integer value to be saved
     */
    public void saveInteger(String pref_name, int pref_value) {
        sp = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
        editor = sp.edit();
        editor.putInt(pref_name, pref_value);
        editor.apply();
    }

    /**
     * Saves string value to the preferences
     * @param pref_name This is the name used to map to a string value
     * @param pref_value This be teh string value to be saved
     */
    public void saveString(String pref_name, String pref_value) {
        sp = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
        editor = sp.edit();
        editor.putString(pref_name, pref_value);
        editor.apply();
    }

    /**
     * Saves float value to the preferences
     * @param pref_name This is the name used to map to a float value
     * @param pref_value This be the float value to be saved
     */
    public void saveFloat(String pref_name, float pref_value) {
        sp = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
        editor = sp.edit();
        editor.putFloat(pref_name, pref_value);
        editor.apply();
    }

    /**
     * Saves long value to the preferences
     * @param pref_name This is the name used to map to a long value
     * @param pref_value This be thye long value to be saved
     */
    public void saveLong(String pref_name, long pref_value) {
        sp = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
        editor = sp.edit();
        editor.putLong(pref_name, pref_value);
        editor.apply();
    }

    /**
     * Loads boolean
     * @param pref_name This be the name used when mapping to the boolean
     * @return Loads the boolean value in the preferences
     */
    public boolean getBoolean(String pref_name) {
        sp = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
        return sp.getBoolean(pref_name, false);
    }

    /**
     * Loads integer
     * @param pref_name This be the name used when mapping to the integer
     * @return Loads the integer value in the preferences
     */
    public int getInteger(String pref_name) {
        sp = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
        return sp.getInt(pref_name, 0);
    }

    /**
     * Loads string
     * @param pref_name This be the name used when mapping to the string
     * @return Loads the string value in the preferences
     */
    public String getString(String pref_name) {
        sp = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
        return sp.getString(pref_name, null);
    }

    /**
     * Loads float
     * @param pref_name This be the name used when mapping to the float
     * @return Loads the float value in the preferences
     */
    public float getFloat(String pref_name) {
        sp = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
        return sp.getFloat(pref_name, 0.0f);
    }

    /**
     * Loads long
     * @param pref_name This be the name used when mapping to the long
     * @return Loads the long value in the preferences
     */
    public long getLong(String pref_name) {
        sp = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
        return sp.getLong(pref_name, 0);
    }

    /**
     * This one clears the preferences saved in the prefs xml file
     */
    public void clearPrefs() {
        sp = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
        editor = sp.edit();
        editor.clear();
        editor.apply();
    }
}
