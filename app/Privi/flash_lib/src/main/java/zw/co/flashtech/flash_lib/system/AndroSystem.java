package zw.co.flashtech.flash_lib.system;

import android.content.Context;
import android.telephony.TelephonyManager;

/**
 * Created by john on 2017/01/06.
 */

public class AndroSystem {
    public static String getIMEI(Context context) {
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return manager.getDeviceId() != null ? manager.getDeviceId() : "unknown device";
    }
}
