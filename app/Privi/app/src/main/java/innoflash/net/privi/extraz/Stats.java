package innoflash.net.privi.extraz;

import innoflash.net.privi.R;

public class Stats {
    public static final String PREF_NAME = "privi_praf_name";
    public static final String DID_WELCOME = "did_welcome";
    public static final String ENJOY = "Enjoy Privi";
    public static final String AUTHENTICATED = "privi_authenticated";
    public static final String FILL_BLANKS = "Please fill in all required blanks";
    public static final String NETWORK_ERROR = "Could not connect to the internet, please try again or check your data !";
    public static final int DIALOG_DURATION = 1000;
    public static final int ERROR_DIALOG_DURATION = 2500;
    public static final String UNEQUAL_PASSWORDS = "Passwords did not match !!!";
    public static final String SHORT_PASSWORD = "Password too short, please enter at least 6 characters";

    public static final String[] MENU_OPTIONS = {
            "Contacts",
            "Call Logs",
            "Requests",
            "SMS Messages"
    };

    public static final int[] MENU_IMAGES = {
            R.drawable.ic_action_users,
            R.drawable.ic_action_clock,
            R.drawable.ic_person_add,
            R.drawable.ic_chat
    };

    public static final String DID_CONTACT_TUTORIAL = "did_privi_contact";
    public static final String DECODING_USER = "Decoding QR Code ..";
    public static final String PLEASE_WAIT = "Please wait";
}
