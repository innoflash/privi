package innoflash.net.privi.extraz;

import android.content.Context;

import com.example.circulardialog.CDialog;
import com.example.circulardialog.extras.CDConstants;

public class Extraz {
    public static void showSuccess(Context context, String message) {
        new CDialog(context).createAlert(message,
                CDConstants.SUCCESS,   // Type of dialog
                CDConstants.LARGE)    //  size of dialog
                .setAnimation(CDConstants.SCALE_FROM_TOP_TO_TOP)     //  Animation for enter/exit
                .setDuration(Stats.DIALOG_DURATION)   // in milliseconds
                .setTextSize(CDConstants.LARGE_TEXT_SIZE)  // CDConstants.LARGE_TEXT_SIZE, CDConstants.NORMAL_TEXT_SIZE
                .show();
    }

    public static void showWarning(Context context, String message) {
        new CDialog(context).createAlert(message,
                CDConstants.WARNING,   // Type of dialog
                CDConstants.LARGE)    //  size of dialog
                .setAnimation(CDConstants.SCALE_FROM_LEFT_TO_RIGHT)     //  Animation for enter/exit
                .setDuration(Stats.DIALOG_DURATION)   // in milliseconds
                .setTextSize(CDConstants.LARGE_TEXT_SIZE)  // CDConstants.LARGE_TEXT_SIZE, CDConstants.NORMAL_TEXT_SIZE
                .show();
    }
    public static void showError(Context context, String message) {
        new CDialog(context).createAlert(message,
                CDConstants.ERROR,   // Type of dialog
                CDConstants.LARGE)    //  size of dialog
                .setAnimation(CDConstants.SCALE_FROM_TOP_TO_BOTTOM)     //  Animation for enter/exit
                .setDuration(Stats.ERROR_DIALOG_DURATION)   // in milliseconds
                .setTextSize(CDConstants.LARGE_TEXT_SIZE)  // CDConstants.LARGE_TEXT_SIZE, CDConstants.NORMAL_TEXT_SIZE
                .show();
    }
}
