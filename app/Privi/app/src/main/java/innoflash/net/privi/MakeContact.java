package innoflash.net.privi;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.budiyev.android.codescanner.AutoFocusMode;
import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.budiyev.android.codescanner.ErrorCallback;
import com.budiyev.android.codescanner.ScanMode;
import com.google.zxing.Result;

import java.util.HashMap;
import java.util.Map;

import innoflash.net.privi.connect.Call;
import innoflash.net.privi.extraz.Extraz;
import innoflash.net.privi.extraz.PriviAPI;
import innoflash.net.privi.extraz.Stats;
import innoflash.net.privi.extraz.VolleySingleton;
import innoflash.net.privi.models.User;
import innoflash.net.privi.showcases.ContactTutorial;
import zw.co.flashtech.flash_lib.preferences.Prefs;

public class MakeContact extends AppCompatActivity implements ErrorCallback, View.OnClickListener, Response.Listener<String>, Response.ErrorListener, DecodeCallback {

    private Toolbar toolbar;
    private Prefs prefs;
    private Intent intent;
    private CodeScanner codeScanner;
    private CodeScannerView scannerView;
    private LinearLayout buttons;
    private LinearLayout scanning;
    private Button rescan;
    private Button proceed;
    private Context context;
    private String scannedID;
    private StringBuilder stringBuilder;
    private RequestQueue requestQueue;
    private StringRequest stringRequest;
    private Map<String, String> params;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.make_contact_activity);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        buttons = findViewById(R.id.buttons);
        scanning = findViewById(R.id.scanning);
        rescan = findViewById(R.id.rescan);
        proceed = findViewById(R.id.proceed);
        scannerView = findViewById(R.id.scanner_view);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        prefs = new Prefs(this, Stats.PREF_NAME);
        codeScanner = new CodeScanner(this, scannerView);
        this.context = this;
        requestQueue = VolleySingleton.getInstance().getRequestQueue();
        progressDialog = new ProgressDialog(this);

        codeScanner.setAutoFocusEnabled(true);
        codeScanner.setCamera(CodeScanner.CAMERA_BACK);
        codeScanner.setAutoFocusMode(AutoFocusMode.SAFE);
        codeScanner.setFormats(CodeScanner.ALL_FORMATS);
        codeScanner.setFlashEnabled(false);
        codeScanner.setScanMode(ScanMode.SINGLE);
        codeScanner.setDecodeCallback(this);
        codeScanner.setErrorCallback(this);
        scannerView.setOnClickListener(this);
        rescan.setOnClickListener(this);
        proceed.setOnClickListener(this);

        if (!prefs.getBoolean(Stats.DID_CONTACT_TUTORIAL)) {
            intent = new Intent(this, ContactTutorial.class);
            startActivity(intent);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        codeScanner.startPreview();
    }

    @Override
    protected void onPause() {
        codeScanner.releaseResources();
        super.onPause();
    }
    @Override
    public void onDecoded(@NonNull final Result result) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                codeScanner.releaseResources();
                codeScanner.stopPreview();

                scannedID = result.getText();
                scanning.setVisibility(View.GONE);
                buttons.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onError(@NonNull final Exception error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Extraz.showError(context, error.getMessage() + "\nYou need to allow the CAMERA permission in your device settings");
            }
        });
        Log.d("camera error", error.getMessage());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.scanner_view:
                codeScanner.startPreview();
                scanning.setVisibility(View.VISIBLE);
                break;
            case R.id.rescan:
                buttons.setVisibility(View.GONE);
                scanning.setVisibility(View.VISIBLE);
                codeScanner.startPreview();
                break;
            case R.id.proceed:
                progressDialog.setTitle(Stats.PLEASE_WAIT);
                progressDialog.setMessage(Stats.DECODING_USER);
              //  progressDialog.show();

                stringBuilder = new StringBuilder(scannedID);
                scannedID = stringBuilder.reverse().toString();
                requestQueue.getCache().clear();
                stringRequest = new StringRequest(Request.Method.POST, PriviAPI.GET_USER, this, this){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        params = new HashMap<>();
                        params.put(User.PRIVI_ID, scannedID);
                        return params;
                    }
                };
                stringRequest.setShouldCache(false);
               // requestQueue.add(stringRequest);
                startActivity(new Intent(this, Call.class));
                break;
        }
    }

    @Override
    public void onResponse(String s) {
        progressDialog.hide();
        Log.i("user", s);
    }

    @Override
    public void onErrorResponse(VolleyError volleyError) {
        progressDialog.hide();
        Extraz.showError(this, volleyError.getMessage());
    }
}
