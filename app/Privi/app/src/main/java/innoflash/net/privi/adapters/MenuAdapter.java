package innoflash.net.privi.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import innoflash.net.privi.R;
import innoflash.net.privi.extraz.Stats;
import innoflash.net.privi.holders.MenuHolder;

public class MenuAdapter extends BaseAdapter{

    private Context context;
    private LayoutInflater inflater;
    private MenuHolder menuHolder;

    public MenuAdapter(Context context) {
        this.context = context;
        this.inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return Stats.MENU_OPTIONS.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            menuHolder = new MenuHolder();
            view = inflater.inflate(R.layout.menu_item, null);
            menuHolder.menu_image = view.findViewById(R.id.menu_image);
            menuHolder.menu_text = view.findViewById(R.id.menu_text);

            view.setTag(menuHolder);
        }else{
            menuHolder = (MenuHolder) view.getTag();
        }

        menuHolder.menu_image.setImageResource(Stats.MENU_IMAGES[i]);
        menuHolder.menu_text.setText(Stats.MENU_OPTIONS[i]);

        return view;
    }
}
