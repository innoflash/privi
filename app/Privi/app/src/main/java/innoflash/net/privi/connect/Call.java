package innoflash.net.privi.connect;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telecom.Connection;
import android.telecom.DisconnectCause;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.abdularis.civ.CircleImageView;

import innoflash.net.privi.R;
import innoflash.net.privi.extraz.Extraz;
import innoflash.net.privi.services.MyConnectionService;
import zw.co.flashtech.flash_lib.notifications.ToastIt;

public class Call extends AppCompatActivity implements View.OnClickListener {

    private CircleImageView profilePicture;
    private FloatingActionButton fab;
    private TextView provider;
    private TextView duration;
    private TextView username;
    private ImageView loud_phone;
    private ImageView hold_phone;
    private Button end_call;
    private TelecomManager telecomManager;
    private PhoneAccountHandle handle;
    private PhoneAccount phoneAccount;
    private DisconnectCause disconnectCause;
    private Connection connection;
    private Bundle callInfoBundle;
    private Bundle callInfo;
    private Uri uri;

    @SuppressLint("NewApi")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.call_activity);

        profilePicture = findViewById(R.id.profilePicture);
        provider = findViewById(R.id.provider);
        duration = findViewById(R.id.duration);
        username = findViewById(R.id.username);
        loud_phone = findViewById(R.id.loud_phone);
        hold_phone = findViewById(R.id.hold_phone);
        end_call = findViewById(R.id.end_call);
        fab = findViewById(R.id.fab);

        end_call.setOnClickListener(this);
        loud_phone.setOnClickListener(this);
        hold_phone.setOnClickListener(this);
        fab.setOnClickListener(this);

        telecomManager = (TelecomManager) this.getSystemService(TELECOM_SERVICE);
        handle = new PhoneAccountHandle(new ComponentName(this, MyConnectionService.class), getString(R.string.app_name));
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            phoneAccount = new PhoneAccount.Builder(handle, getString(R.string.app_name))
                    .setCapabilities(PhoneAccount.CAPABILITY_SELF_MANAGED)
                    .build();
            telecomManager.registerPhoneAccount(phoneAccount);
        }
        phoneAccount = new PhoneAccount.Builder(handle, getString(R.string.app_name))
                .setCapabilities(PhoneAccount.CAPABILITY_CALL_PROVIDER)
                .build();
        telecomManager.registerPhoneAccount(phoneAccount);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.end_call:
                makeCall();
                break;
            case R.id.loud_phone:
                endCall();
                break;
            case R.id.hold_phone:
                break;
            case R.id.fab:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        //todo start a call
    }

    @Override
    protected void onPause() {
        super.onPause();
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        //todo end a call
        super.onBackPressed();
    }


    @SuppressLint("NewApi")
    private void makeCall() {
        connection = MyConnectionService.getConnection();
        if (connection != null) {
            if (connection.getState() == Connection.STATE_ACTIVE) {
                ToastIt.message(this, "active call");
            } else if (connection.getState() == Connection.STATE_DIALING) {
                ToastIt.message(this, "Cant place call, you are still dialing");
            } else {
                Extraz.showError(this, "You cannot make a phone call right now");
            }
        } else {
            uri = Uri.parse("tel:+2784325498");
            callInfoBundle = new Bundle();
            callInfo = new Bundle();
            callInfoBundle.putString("to", "+2784325498");

            callInfo.putParcelable(TelecomManager.EXTRA_OUTGOING_CALL_EXTRAS, callInfoBundle);
            callInfo.putParcelable(TelecomManager.EXTRA_PHONE_ACCOUNT_HANDLE, handle);
            callInfo.putBoolean(TelecomManager.EXTRA_START_CALL_WITH_VIDEO_STATE, true);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                Extraz.showError(this, "Calling permission not granted");
                return;
            } else {
                telecomManager.placeCall(uri, callInfo);
            }

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void endCall() {
        Connection conn = MyConnectionService.getConnection();
        if (conn == null) {
            Extraz.showError(this, "No call exists for you to end");
        } else {
            disconnectCause = new DisconnectCause(DisconnectCause.LOCAL);
            conn.setDisconnected(disconnectCause);
            conn.destroy();
            MyConnectionService.deinitConnection();

        }
    }
}
