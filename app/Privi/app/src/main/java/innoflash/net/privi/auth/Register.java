package innoflash.net.privi.auth;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.AccelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.HashMap;
import java.util.Map;

import innoflash.net.privi.R;
import innoflash.net.privi.extraz.Extraz;
import innoflash.net.privi.extraz.PriviAPI;
import innoflash.net.privi.extraz.Stats;
import innoflash.net.privi.extraz.VolleySingleton;
import innoflash.net.privi.models.User;

public class Register extends AppCompatActivity implements View.OnClickListener, Response.ErrorListener, Response.Listener<String> {

    private FloatingActionButton fab;
    private MKLoader loader;
    private EditText et_first_name;
    private EditText et_last_name;
    private EditText et_phone;
    private EditText et_email;
    private EditText et_password;
    private EditText et_conf_password;
    private Button button;
    private CardView cvAdd;
    private Transition transition;
    private Animator mAnimator;
    private RequestQueue requestQueue;
    private StringRequest stringRequest;
    private Map<String, String> params;
    private String first_name;
    private String last_name;
    private String phone;
    private String email;
    private String password;
    private String conf_password;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        showEnterAnimation();
        fab = findViewById(R.id.fab);
        cvAdd = findViewById(R.id.cv_add);
        loader = findViewById(R.id.loader);
        button = findViewById(R.id.bt_go);
        et_first_name = findViewById(R.id.et_first_name);
        et_last_name = findViewById(R.id.et_last_name);
        et_phone = findViewById(R.id.et_phone);
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        et_conf_password = findViewById(R.id.et_repeatpassword);

        requestQueue = VolleySingleton.getInstance().getRequestQueue();

        fab.setOnClickListener(this);
        button.setOnClickListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void showEnterAnimation() {
        transition = TransitionInflater.from(this).inflateTransition(R.transition.fabtransition);
        getWindow().setSharedElementEnterTransition(transition);

        transition.addListener(new Transition.TransitionListener() {
            @Override
            public void onTransitionStart(Transition transition) {
                cvAdd.setVisibility(View.GONE);
            }

            @Override
            public void onTransitionEnd(Transition transition) {
                transition.removeListener(this);
                animateRevealShow();
            }

            @Override
            public void onTransitionCancel(Transition transition) {

            }

            @Override
            public void onTransitionPause(Transition transition) {

            }

            @Override
            public void onTransitionResume(Transition transition) {

            }


        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void animateRevealShow() {
        mAnimator = ViewAnimationUtils.createCircularReveal(cvAdd, cvAdd.getWidth() / 2, 0, fab.getWidth() / 2, cvAdd.getHeight());
        mAnimator.setDuration(500);
        mAnimator.setInterpolator(new AccelerateInterpolator());
        mAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
            }

            @Override
            public void onAnimationStart(Animator animation) {
                cvAdd.setVisibility(View.VISIBLE);
                super.onAnimationStart(animation);
            }
        });
        mAnimator.start();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab:
                animateRevealClose();
                break;
            case R.id.bt_go:
                first_name = et_first_name.getText().toString();
                last_name = et_last_name.getText().toString();
                phone = et_phone.getText().toString();
                email = et_email.getText().toString();
                password = et_password.getText().toString();
                conf_password = et_conf_password.getText().toString();
                if (password.compareTo(conf_password) == 0) {
                    if (first_name.length() == 0 || last_name.length() == 0 || phone.length() == 0) {
                        Extraz.showWarning(this, Stats.FILL_BLANKS);
                    } else {
                        if (password.length() > 5) {
                            requestQueue.getCache().clear();
                            stringRequest = new StringRequest(Request.Method.POST, PriviAPI.REGISTER, this, this) {
                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    params = new HashMap<>();
                                    params.put(User.FIRST_NAME, first_name);
                                    params.put(User.LAST_NAME, last_name);
                                    params.put(User.PHONE, phone);
                                    params.put(User.EMAIL, email);
                                    params.put(User.PASSWORD, password);
                                    return params;
                                }
                            };
                            stringRequest.setShouldCache(false);
                            loader.setVisibility(View.VISIBLE);
                            requestQueue.add(stringRequest);
                        } else {
                            Extraz.showError(this, Stats.SHORT_PASSWORD);
                        }
                    }

                } else {
                    Extraz.showError(this, Stats.UNEQUAL_PASSWORDS);
                }
                break;
            default:
                //default l
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void animateRevealClose() {
        mAnimator = ViewAnimationUtils.createCircularReveal(cvAdd, cvAdd.getWidth() / 2, 0, cvAdd.getHeight(), fab.getWidth() / 2);
        mAnimator.setDuration(500);
        mAnimator.setInterpolator(new AccelerateInterpolator());
        mAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                cvAdd.setVisibility(View.INVISIBLE);
                super.onAnimationEnd(animation);
                fab.setImageResource(R.drawable.plus);
                Register.super.onBackPressed();
            }

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
            }
        });
        mAnimator.start();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBackPressed() {
        animateRevealClose();
    }

    @Override
    public void onErrorResponse(VolleyError volleyError) {
        loader.setVisibility(View.GONE);
        Extraz.showError(this, Stats.NETWORK_ERROR);
    }

    @Override
    public void onResponse(String s) {
        loader.setVisibility(View.GONE);
    }
}
