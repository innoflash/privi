package innoflash.net.privi.extraz;

public class PriviAPI {
    public static final String API_ROOT = "https://api.privi.net/api/";
    public static final String LOGIN = API_ROOT + "privi-signin";
    public static final String REGISTER = API_ROOT + "privi-register";
    public static final String GET_USER = API_ROOT + "privi-getprivi";
}
