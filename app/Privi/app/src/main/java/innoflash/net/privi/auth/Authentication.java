package innoflash.net.privi.auth;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.HashMap;
import java.util.Map;

import innoflash.net.privi.R;
import innoflash.net.privi.extraz.Extraz;
import innoflash.net.privi.extraz.PriviAPI;
import innoflash.net.privi.extraz.Stats;
import innoflash.net.privi.extraz.VolleySingleton;
import innoflash.net.privi.models.User;
import zw.co.flashtech.flash_lib.preferences.Prefs;

public class Authentication extends AppCompatActivity implements View.OnClickListener, Response.ErrorListener, Response.Listener<String> {

    private EditText etUsername;
    private EditText etPassword;
    private TextView forgot_password;
    private MKLoader loader;
    private Prefs prefs;
    private Button btGo;
    private CardView cv;
    private FloatingActionButton fab;
    private ActivityOptions options;
    private Intent intent;
    private String user_phone;
    private String user_password;
    private RequestQueue requestQueue;
    private StringRequest stringRequest;
    private Map<String, String> params;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.authentication_activity);

        prefs = new Prefs(this, Stats.PREF_NAME);

        requestQueue = VolleySingleton.getInstance().getRequestQueue();
        etUsername = findViewById(R.id.et_phone);
        etPassword = findViewById(R.id.et_password);
        loader = findViewById(R.id.loader);
        btGo = findViewById(R.id.bt_go);
        cv = findViewById(R.id.cv);
        fab = findViewById(R.id.fab);
        forgot_password = findViewById(R.id.forgot_password);

        btGo.setOnClickListener(this);
        fab.setOnClickListener(this);
        forgot_password.setOnClickListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_go:
                user_phone = etUsername.getText().toString();
                user_password = etPassword.getText().toString();
                if (user_phone.length() == 0 || user_password.length() == 0) {
                    Extraz.showWarning(this, Stats.FILL_BLANKS);
                } else {
                    requestQueue.getCache().clear();
                    stringRequest = new StringRequest(Request.Method.POST, PriviAPI.LOGIN, this, this) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            params = new HashMap<>();
                            params.put(User.PHONE, user_phone);
                            params.put(User.PASSWORD, user_password);
                            return params;
                        }
                    };
                    loader.setVisibility(View.VISIBLE);
                    stringRequest.setShouldCache(false);
                    requestQueue.add(stringRequest);
                }
                break;
            case R.id.fab:
                getWindow().setExitTransition(null);
                getWindow().setEnterTransition(null);
                options = ActivityOptions.makeSceneTransitionAnimation(this, fab, fab.getTransitionName());
                intent = new Intent(this, Register.class);
                startActivity(intent, options.toBundle());
                break;
            case R.id.forgot_password:
                prefs.saveBoolean(Stats.DID_WELCOME, true);
                prefs.saveBoolean(Stats.AUTHENTICATED, true);
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @SuppressLint("RestrictedApi")
    @Override
    protected void onRestart() {
        super.onRestart();
        fab.setVisibility(View.GONE);
    }

    @SuppressLint("RestrictedApi")
    @Override
    protected void onResume() {
        super.onResume();
        fab.setVisibility(View.VISIBLE);
    }

    @Override
    public void onErrorResponse(VolleyError volleyError) {
        loader.setVisibility(View.GONE);
        Extraz.showError(this, Stats.NETWORK_ERROR);
    }

    @Override
    public void onResponse(String s) {
        loader.setVisibility(View.GONE);
    }
}
