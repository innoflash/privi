package innoflash.net.privi.services;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.telecom.Connection;
import android.telecom.ConnectionRequest;
import android.telecom.ConnectionService;
import android.telecom.DisconnectCause;
import android.telecom.PhoneAccountHandle;
import android.telecom.StatusHints;
import android.telecom.TelecomManager;

import innoflash.net.privi.connect.Call;

@TargetApi(Build.VERSION_CODES.M)
public class MyConnectionService extends ConnectionService {

    private static String TAG = "MyConnectionService";
    private static Connection conn;
    private DisconnectCause disconnectCause;
    private Intent intent;
    private StatusHints statusHints;

    public static Connection getConnection() {
        return conn;
    }

    public static void deinitConnection() {
        conn = null;
    }

    @SuppressLint("NewApi")
    @Override
    public Connection onCreateOutgoingConnection(PhoneAccountHandle connectionManagerPhoneAccount, ConnectionRequest request) {
        Connection connection = new Connection() {
            @Override
            public void onAnswer() {
                super.onAnswer();
            }

            @Override
            public void onReject() {
                super.onReject();
            }

            @Override
            public void onReject(String replyMessage) {
                super.onReject(replyMessage);
            }

            @Override
            public void onAbort() {
                super.onAbort();
            }

            @Override
            public void onDisconnect() {
                disconnectCause = new DisconnectCause(DisconnectCause.LOCAL);
                this.setDisconnected(disconnectCause);
                this.destroy();
                conn = null;
                //    super.onDisconnect();
            }

            @Override
            public void onStateChanged(int state) {
                if (state == Connection.STATE_DIALING) {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            intent = new Intent(getApplicationContext(), Call.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            getApplication().startActivity(intent);
                        }
                    }, 500);
                }
                //     super.onStateChanged(state);
            }
        };

        connection.setConnectionProperties(Connection.PROPERTY_SELF_MANAGED);
        connection.setConnectionCapabilities(Connection.CAPABILITY_HOLD);
        connection.setConnectionCapabilities(Connection.CAPABILITY_SUPPORT_HOLD);
        connection.setAddress(Uri.parse("tel:+2784325498"), TelecomManager.PRESENTATION_ALLOWED);
        connection.setDialing();
        conn = connection;
        return connection;
    }

    @Override
    public void onCreateOutgoingConnectionFailed(PhoneAccountHandle connectionManagerPhoneAccount, ConnectionRequest request) {
        super.onCreateOutgoingConnectionFailed(connectionManagerPhoneAccount, request);
    }
}
