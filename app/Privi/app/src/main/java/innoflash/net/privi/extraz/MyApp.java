package innoflash.net.privi.extraz;

import android.app.Application;
import android.content.Context;

public class MyApp extends Application {
    private static MyApp myApp;
    private static Context appContext;

    public static Context getAppContext() {
        return myApp.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        myApp = this;
    }

    public static MyApp getInstance() {
        return myApp;
    }
}
