package innoflash.net.privi;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.transition.Explode;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.jpeng.jpspringmenu.MenuListener;
import com.jpeng.jpspringmenu.SpringMenu;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

import innoflash.net.privi.adapters.MenuAdapter;
import innoflash.net.privi.auth.Authentication;
import innoflash.net.privi.extraz.Extraz;
import innoflash.net.privi.extraz.Stats;
import innoflash.net.privi.showcases.Welcome;
import zw.co.flashtech.flash_lib.notifications.ToastIt;
import zw.co.flashtech.flash_lib.preferences.Prefs;

public class Privi extends AppCompatActivity implements MenuListener, View.OnClickListener, AdapterView.OnItemClickListener, MultiplePermissionsListener, PermissionRequestErrorListener {
    private Prefs prefs;
    private Intent intent;
    private Explode explode;
    private SpringMenu springMenu;
    private Button openMenu;
    private MenuAdapter menuAdapter;
    private ListView priviMenu;
    private LinearLayout logout;
    private ImageView makeContact;
    private ImageView webEmbed;
    private ImageView shareCode;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privi);
        prefs = new Prefs(this, Stats.PREF_NAME);
        menuAdapter = new MenuAdapter(this);

        springMenu = new SpringMenu(this, R.layout.privi_menu);
        openMenu = findViewById(R.id.openMenu);
        makeContact = findViewById(R.id.make_contact);
        webEmbed = findViewById(R.id.web_embed);
        shareCode = findViewById(R.id.share_code);
        priviMenu = springMenu.findViewById(R.id.privi_menu);
        logout = springMenu.findViewById(R.id.logout);

        explode = new Explode();
        explode.setDuration(500);
        getWindow().setExitTransition(explode);
        getWindow().setEnterTransition(explode);
        priviMenu.setAdapter(menuAdapter);

        springMenu.setMenuListener(this);
        springMenu.setFadeEnable(true);
        springMenu.setDragOffset(0.4f);
        springMenu.setDirection(SpringMenu.DIRECTION_LEFT);
        //  springMenu.setChildSpringConfig(SpringConfig.fromOrigamiTensionAndFriction(20, 5));

        openMenu.setOnClickListener(this);
        logout.setOnClickListener(this);
        makeContact.setOnClickListener(this);
        webEmbed.setOnClickListener(this);
        shareCode.setOnClickListener(this);
        priviMenu.setOnItemClickListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onStart() {
        super.onStart();
        if (!prefs.getBoolean(Stats.DID_WELCOME)) {
            intent = new Intent(this, Welcome.class);
            startActivity(intent);
        } else {
            if (!prefs.getBoolean(Stats.AUTHENTICATED)) {
                intent = new Intent(this, Authentication.class);
                startActivity(intent);
            } else {
                Dexter.withActivity(this)
                        .withPermissions(Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.CALL_PRIVILEGED,
                                Manifest.permission.CALL_PHONE,
                                Manifest.permission.MANAGE_OWN_CALLS)
                        .withListener(this)
                        .withErrorListener(this)
                        .check();
            }
        }
    }

    @Override
    public void onMenuOpen() {

    }

    @Override
    public void onMenuClose() {

    }

    @Override
    public void onProgressUpdate(float value, boolean bouncing) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.openMenu:
                springMenu.openMenu();
                break;
            case R.id.logout:
                prefs.saveBoolean(Stats.AUTHENTICATED, false);
                finish();
                break;
            case R.id.make_contact:
                intent = new Intent(this, MakeContact.class);
                startActivity(intent);
                break;
            case R.id.web_embed:
                intent = new Intent(this, WebEmbed.class);
                startActivity(intent);
                break;
            case R.id.share_code:
                //todo share png with other apps
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        switch (i) {
            case 0:
                intent = new Intent(this, Contacts.class);
                break;
            case 1:
                intent = new Intent(this, CallLogs.class);
                break;
            case 2:
                intent = new Intent(this, Requests.class);
                break;
            case 3:
                intent = new Intent(this, SMSMessages.class);
                break;
        }
        this.startActivity(intent);
    }

    @Override
    public void onPermissionsChecked(MultiplePermissionsReport report) {
        for (PermissionGrantedResponse response : report.getGrantedPermissionResponses()) {
            /*ToastIt.message(this, response.getPermissionName() + " permission granted");
            Extraz.showSuccess(this, response.getPermissionName() + " permission granted");*/
        }

        for (PermissionDeniedResponse response : report.getDeniedPermissionResponses()) {
            ToastIt.message(this, response.getPermissionName() + " is denied");
        }
    }

    @Override
    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

    }

    @Override
    public void onError(DexterError error) {
        Extraz.showError(this, error.toString());
    }
}
