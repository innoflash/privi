package innoflash.net.privi.models;

public class User {
    public static final String PHONE = "phone";
    public static final String PASSWORD = "password";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String EMAIL = "email";
    public static final String PRIVI_ID = "privi_id";
}
