<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {
    $api->get('/test', function(){
        return 'Hellow';
    });

    $api->post('/privi-register', 'App\Http\Controllers\PriviController@register');
    $api->post('/privi-signin', 'App\Http\Controllers\PriviController@signin');
    $api->post('/privi-activate', 'App\Http\Controllers\PriviController@activatePrivi');
    $api->post('/privi-updatepassword', 'App\Http\Controllers\PriviController@updatePassword');
    $api->post('/privi-resendactivation', 'App\Http\Controllers\PriviController@resendActivation');
    $api->post('/privi-forgotpassword', 'App\Http\Controllers\PriviController@forgotPassword');
    $api->post('/privi-getprivi', 'App\Http\Controllers\PriviController@getPrivi');
    $api->get('/try', 'App\Http\Controllers\PriviController@tryIt');
});
