<?php

namespace App\Http\Controllers;

use App\Code;
use App\Privi;
use App\StringFormatter;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use QR_Code\QR_Code;
use function array_splice;
use function implode;
use function rand;
use function shuffle;
use function sizeof;
use function str_replace;
use function str_split;
use function strcmp;
use function strrev;
use function time;

class PriviController extends Controller
{
    private $twistedID = '';

    function register(Request $request)
    {

        $response = [];
        $privi = new Privi();
        $privi->first_name = $request->first_name;
        $privi->last_name = $request->last_name;
        $privi->phone = $this->makeNumber($request->phone);
        $privi->email = $request->email;
        $privi->qr_code = $this->makePicName($request->first_name . $request->last_name);
        $privi->password = md5($request->password);
        $privi->privi_id = $this->getID($request->first_name, $request->last_name);

        try {
            $privi->save();
            QR_Code::png($privi->privi_id, base_path('storage/app/public/' . $privi->qr_code . '.png'), '', 5, 0);

            $code = new Code();
            $code->code = rand(1111111, 9999999);

            $privi->code()->save($code);
            $response['success'] = true;
            $response['message'] = 'You have been registered successfully, please proceed with using the activation code sent to your cell!';
            //todo send activation code to user
        } catch (Exception $e) {
            $response['success'] = false;
            $response['message'] = $e->getMessage();
        }
        return $privi;
    }

    function signin(Request $request)
    {
        $response = [];
        $user = Privi::where([
            'phone' => $this->makeNumber($request->phone),
            'password' => md5($request->password),
        ])->first();

        if ($user == NULL) {
            $response['success'] = false;
            $response['message'] = 'Invalid user credentials or you need to activate your account';
        } else {

            if ($user->active == true) {
                $response['success'] = true;
                $response['message'] = 'Welcome back ' . $user->first_name;
                $response['activate'] = false;
                $response['user'] = $user;
            } else {
                $response['success'] = true;
                $response['activate'] = true;
                $response['message'] = 'Your login credentials are valid, but may you please activate your account first!';
            }

        }
        return $response;
    }

    function activatePrivi(Request $request)
    {
        $user = Privi::findOrFail($request->user_id);
        $code = $user->code;
        if (strcmp($request->code, $code->code) == 0) {
            $user->active = true;
            try {
                $user->save();
                $response['success'] = true;
                $response['message'] = 'Your account activation was successful, welcome to Privi!';
            } catch (Exception $e) {
                $response['success'] = false;
                $response['message'] = $e->getMessage();
            }

        } else {
            $response['success'] = false;
            $response['message'] = 'You have used an invalid activation code, please try again';
        }
        return $response;
    }

    function updatePassword(Request $request)
    {
        $user = Privi::where([
            'privi_id' => $request->privi_id,
            'password' => md5($request->password)
        ])->first();

        if ($user == NULL) {
            $response['success'] = false;
            $response['message'] = 'You have entered a wrong current password';
        } else {
            try {
                DB::table('privis')
                    ->where('privi_id', $request->privi_id)
                    ->update([
                        'password' => md5($request->new_password),
                        'updated_at' => Carbon::now()
                    ]);
                $response['success'] = true;
                $response['message'] = 'Password changed successfully';
            } catch (\Exception $e) {
                $response['success'] = false;
                $response['message'] = $e->getMessage();
            }
        }
        return $response;
    }

    function resendActivation(Request $request)
    {
        $user = Privi::findOrFail($request->privi_id);
        $credentials = "tswelelopiet@gmail.com:dee2e502-3cbb-42e7-b73d-abbe07dd0fb6";

        $url = "https://www.zoomconnect.com/app/api/rest/v1/sms/send.json";

        $data = new stdClass();
        $data->message = 'Hi ' . $user->first_name . '. you requested for your activation code and here is it : ' . $user->activationCode->code;
        $data->recipientNumber = $user->phone;

        $data_string = json_encode($data);

        $result = file_get_contents($url, null, stream_context_create(array(
            'http' => array(
                'method' => 'POST',
                'header' => "Content-type: application/json\r\n" .
                    "Connection: close\r\n" .
                    "Content-length: " . strlen($data_string) . "\r\n" .
                    "Authorization: Basic " . base64_encode($credentials) . "\r\n",
                'content' => $data_string,
            ),
        )));

        if ($result) {
            $response['success'] = true;
            $response['message'] = 'An activation code has been send to you, open your SMS app to retrieve it!';
        } else {
            $response['success'] = false;
            $response['message'] = 'Something went wrong from our side, please try again later';
        }

        return $response;
        //todo refactor this code
    }

    function forgotPassword(Request $request)
    {
        $user = Privi::where([
            'phone' => $this->makeNumber($request->phone)
        ])->first();
        if ($user == NULL) {
            $response['success'] = false;
            $response['message'] = 'An account with this phone number was not found!';
        } else {
            //event(new ForgotPassword($user));
            //todo send a new password to the user
            $response['success'] = true;
            $response['message'] = 'We have reset your password to our default one, we sent you the new password to login but don`t forget to change it once you are logged in';
        }
        return $response;
    }

    function getPrivi(Request $request)
    {
        $user = Privi::where('privi_id', strrev($request->privi_id))->first();

        if ($user == NULL) {
            $response['success'] = false;
            $response['message'] = 'No user found with this ID';
        } else {
            $response['success'] = true;
            $response['message'] = 'User found';
            $response['user'] = $user;
        }
        return $response;
    }


    private function getID($first_name, $last_name)
    {
        $longString = $first_name . time() . $last_name . 'privi';
        $longString = StringFormatter::lowercase($longString);
        $longString = str_replace(' ', '', $longString);

        $strArray = str_split($longString);
        shuffle($strArray);
        shuffle($strArray);
        shuffle($strArray);
        return implode('', $strArray);
    }

    private function makePicName($name)
    {
        $picname = time() . date('YmdHis') . $name;
        $picname = StringFormatter::lowercase($picname);
        $picname = str_replace(' ', '', $picname);;
        $picname = str_split($picname);
        shuffle($picname);
        shuffle($picname);
        $picname = implode('', $picname);
        return $picname;
    }

    private function twistChars($array = array())
    {
        $int = rand(0, sizeof($array) - 1);
        if (sizeof($array) != 0) {
            $this->twistedID .= $array[$int];
            $array = array_splice($array, $int, 1);
            $this->twistChars($array);
        } else {

        }
        return $this->twistedID;
    }

    private function makeNumber($number)
    {
        if (strlen($number) <= 10) {
            if (strpos($number, '0') == 0) {
                return str_replace(' ', '', '+27' . substr($number, 1));
            } else {
                //when number is less than 10
            }
        } else {
            return str_replace(' ', '', '+27' . substr($number, strlen($number) - 9));
        }
    }

}
