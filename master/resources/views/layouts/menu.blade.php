<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('img/abiri.jpeg') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ $user->first_name }} {{ $user->last_name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                <a href="/dashboard">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li>
                <a href="/users">
                    <i class="fa fa-group"></i> <span>Users</span>
                </a>
            </li>
            <li>
                <a href="/cars">
                    <i class="fa fa-car"></i> <span>Cars</span>
                </a>
            </li>
            <li>
                <a href="/journeys">
                    <i class="fa fa-road"></i> <span>Journeys</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-map-signs"></i> <span>Main Roads</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/mainroads"><i class="fa fa-circle-o"></i> View Roads</a></li>
                    <li><a href="/addroads"><i class="fa fa-circle-o"></i> Add Roads</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-money"></i> <span>Tollgates</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/tollgates"><i class="fa fa-circle-o"></i> View Tollgates</a></li>
                    <li><a href="/addtollgate"><i class="fa fa-circle-o"></i> Add Tollgate</a></li>
                </ul>
            </li>
            {{--            <li class="treeview">
                            <a href="#">
                                <i class="fa fa-dollar"></i> <span>E-tolls</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/etolls"><i class="fa fa-circle-o"></i> View E-tolls</a></li>
                                <li><a href="/addetoll"><i class="fa fa-circle-o"></i> Add E-toll</a></li>
                            </ul>
                        </li>--}}
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-map"></i> <span>Taxi Ranks</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/taxiranks"><i class="fa fa-circle-o"></i> View Ranks</a></li>
                    <li><a href="/addrank"><i class="fa fa-circle-o"></i> Add Rank</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-tint"></i> <span>Fuel Consumption</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/fuels"><i class="fa fa-circle-o"></i> View Consumptions</a></li>
                    <li><a href="/addfuel"><i class="fa fa-circle-o"></i> Add Consumption</a></li>
                    <li><a href="/costs"><i class="fa fa-circle-o"></i> View Prices</a></li>
                    <li><a href="/addcost"><i class="fa fa-circle-o"></i> Add Price</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-check-circle-o"></i> <span>Feedback</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/feedback"><i class="fa fa-circle-o"></i> New Feedback</a></li>
                    <li><a href="/readfeedback"><i class="fa fa-circle-o"></i> Read Feedback</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-times-circle-o"></i> <span>Reports</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/reports"><i class="fa fa-circle-o"></i> New Reports</a></li>
                    <li><a href="/readreports"><i class="fa fa-circle-o"></i> Read Reports</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-question-circle-o"></i> <span>FAQs</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/faqs"><i class="fa fa-circle-o"></i> View FAQs</a></li>
                    <li><a href="/addfaq"><i class="fa fa-circle-o"></i> Add FAQ</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-question-circle"></i> <span>Queries</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/newqueries"><i class="fa fa-circle-o"></i> New Queries</a></li>
                    <li><a href="/readqueries"><i class="fa fa-circle-o"></i> Read Queries</a></li>
                </ul>
            </li>

            <li>
                <a href="/subscriptions">
                    <i class="fa fa-inbox"></i> <span>Subscriptions</span>
                </a>
            </li>       <li>
                <a href="/logout">
                    <i class="fa fa-lock"></i> <span>Logout</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>s</a>