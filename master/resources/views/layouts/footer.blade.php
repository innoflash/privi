<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.1
    </div>
    <strong>Copyright &copy; {{ date('Y') }} <a href="#">
    {{ $app_name }}    
    </a>.</strong> All rights
    reserved.
</footer>