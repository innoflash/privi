@extends('portal::layouts.auth')
@section('title')
    Registration
@endsection
@section('project_name')
    SoulECards
@endsection
@section('content')
    <div class="register-box-body">
        <p class="login-box-msg">Register a new membership</p>

        <form action="/portal/registration" method="POST" data-parsley-validate>
            {{ csrf_field() }}
            <div class="form-group has-feedback">
                <input type="text" name="first_name" placeholder="First name" value="{{ old('first_name') }}"
                       class="form-control" required
                       data-parsley-trigger="keyup" data-parsley-minlength="3" data-parsley-maxlength="100"
                       data-parsley-minlength-message="You need to enter at least a 3 character for first name"
                       data-parsley-validation-threshold="10">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="text" name="last_name" placeholder="Last name" value="{{ old('last_name') }}"
                       class="form-control" required
                       data-parsley-trigger="keyup" data-parsley-minlength="3" data-parsley-maxlength="100"
                       data-parsley-minlength-message="You need to enter at least a 3 character for last name"
                       data-parsley-validation-threshold="10">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="email" name="email" placeholder="Email address" value="{{ old('email') }}"
                       class="form-control"
                       data-parsley-trigger="change"
                       required>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
                <input type="date" name="dob" placeholder="DOB" value="{{ old('dob') }}" class="form-control" required>
                <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
            </div>

            <h6>Gender</h6>
            <select name="gender" class="form-control">
                <option value="M">Male</option>
                <option value="F">Female</option>
            </select>
            <hr/>
            <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control" placeholder="Password"
                       data-parsley-trigger="keyup" data-parsley-minlength="6" data-parsley-maxlength="100"
                       data-parsley-minlength-message="You need to enter at least a 6 character for password"
                       data-parsley-validation-threshold="10"
                       required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="confirm_password" class="form-control" placeholder="Retype password"
                       data-parsley-trigger="keyup" data-parsley-minlength="6" data-parsley-maxlength="100"
                       data-parsley-minlength-message="You need to enter at least a 6 character for password"
                       data-parsley-validation-threshold="10"
                       required>
                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
            </div>
            <div class="g-recaptcha" data-sitekey="6Le94zoUAAAAAIpPOLuiqaMCQh1cTgA5JV8ozE1x"></div>
            <hr/>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox"> I agree to the <a href="#">terms</a>
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

        <div class="social-auth-links text-center">
            <p>- OR -</p>
            <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up
                using
                Facebook</a>
        </div>

        <a href="/portal/login" class="text-center">I already have a membership</a>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection

