@extends('portal::layouts.auth')
@section('title')
    Forgot Password
@endsection
@section('project_name')
    SoulECards
@endsection
@section('content')
    <div class="login-box-body">
        <p class="login-box-msg">Forgot your password, <br/>enter your email address to reset</p>

        <form class="form-signin" method="POST" data-parsley-validate>
            {{ csrf_field() }}
            <div class="form-group has-feedback">
                <input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}"
                       data-parsley-trigger="change"
                       required autofocus>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-7">

                </div>
                <!-- /.col -->
                <div class="col-xs-5">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Reset Password</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

        <a href="/portal/login">Login</a><br>

    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection
