<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $sentry = new \Cartalyst\Sentry\Sentry();

        try {
            $sentry->register([
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'email' => 'all@gmail.com',
                'password' => 'secret',
                'activated' => true
            ]);
        } catch (Exception $e) {
        }
    }
}
