<?php
/**
 * Created by PhpStorm.
 * User: Flash
 * Date: 11/29/2017
 * Time: 12:10 PM
 */

namespace App;


use Carbon\Carbon;

class StringFormatter
{
    public static function descriptionText($string)
    {
        $string = stripslashes($string);
        $string = trim($string);

        $finds = [
            "'",
            "-"
        ];

        $fix = [
            "`",
            "_"
        ];

        return str_replace($finds, $fix, ucfirst($string));
    }

    static function uppercase($string)
    {
        $string = stripslashes($string);
        $string = trim($string);

        $finds = [
            "'",
            "-"
        ];

        $fix = [
            "`",
            "_"
        ];

        return str_replace($finds, $fix, strtoupper($string));
    }

    static function lowercase($string)
    {
        $string = stripslashes($string);
        $string = trim($string);

        $finds = [
            "'",
            "-"
        ];

        $fix = [
            "`",
            "_"
        ];

        return str_replace($finds, $fix, strtolower($string));
    }

    public static function titleText($string)
    {
        $string = stripslashes($string);
        $string = trim($string);

        $finds = [
            "'",
            "-"
        ];

        $fix = [
            "`",
            "_"
        ];

        return str_replace($finds, $fix, ucwords($string));
    }

    static function humanDate($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->diffForHumans();
    }
}