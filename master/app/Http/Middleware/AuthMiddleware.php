<?php

namespace App\Http\Middleware;

use Cartalyst\Sentry\Sentry;
use Closure;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $sentry = new Sentry();
        if ($sentry->check()) {
            return $next($request);

        } else {
            return redirect('/');
        }
    }
}
