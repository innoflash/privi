<?php

namespace App\Http\Controllers;

use Cartalyst\Sentry\Sentry;
use Cartalyst\Sentry\Users\LoginRequiredException;
use Cartalyst\Sentry\Users\PasswordRequiredException;
use Cartalyst\Sentry\Users\UserNotActivatedException;
use Cartalyst\Sentry\Users\UserNotFoundException;
use Cartalyst\Sentry\Users\WrongPasswordException;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    function index()
    {
        $sentry = new Sentry();
        if ($sentry->check()) {
            return redirect('/dashboard');
        }
        return view('auth.login');
    }

    function login(Request $request)
    {
        $errored = true;
        try {
            $sentry = new Sentry();
            // Login credentials
            $credentials = array(
                'email' => $request->email,
                'password' => $request->password,
            );

            // Authenticate the user
            $user = $sentry->authenticate($credentials, false);
            $errored = false;
            $sentry->login($user, false);
            return redirect('/dashboard');

        } catch (LoginRequiredException $e) {
            $error = 'Login field is required.';
        } catch (PasswordRequiredException $e) {
            $error = 'Password field is required.';
        } catch (WrongPasswordException $e) {
            $error = 'Wrong password, try again.';
        } catch (UserNotFoundException $e) {
            $error = 'User was not found.';
        } catch (UserNotActivatedException $e) {
            $error = 'User is not activated.';
        }
        if ($errored) {
            return redirect()->back()
                ->withInput($request->all())
                ->withErrors([$error]);
        }
    }

    function logout()
    {
        $sentry = new Sentry();
        $sentry->logout();
        return redirect('/');
    }
}
