<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MakePage;

class DashboardController extends Controller
{
    function index(){
        return MakePage::make('index');
    }
}
