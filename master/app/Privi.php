<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Privi extends Model
{
    function getFirstNameAttribute($val)
    {
        return StringFormatter::titleText($val);
    }

    function getLastNameAttribute($val)
    {
        return StringFormatter::titleText($val);
    }


    function code()
    {
        return $this->hasOne(Code::class, 'user_id');
    }

}
