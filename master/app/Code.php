<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    function privi(){
        return $this->belongsTo(Privi::class, 'user_id');
    }
}
