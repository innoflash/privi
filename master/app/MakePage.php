<?php
/**
 * Created by PhpStorm.
 * User: Flash
 * Date: 12/12/2017
 * Time: 11:15 AM
 */

namespace App;

use App\User;
use Cartalyst\Sentry\Sentry;
use Vertopia\Portal\Core as VPC;

class MakePage
{
    static function make($view, $items = [])
    {
        $sentry = new Sentry();
        $user = $sentry->getUser();
        $user = User::findOrFail($user->getId());

        return view($view)
            ->with(array_merge([
                'user' => $user
            ], $items));
    }

    static function getUser()
    {
        $sentry = new Sentry();
        $user = $sentry->getUser();
        $user = User::findOrFail($user->getId());

        return $user;
    }

}