$(document).ready(function (e) {
    $('*#delItem').on('click', function (e) {
        var item = $(this).attr('item');
        var route = $(this).attr('url');

        swal({
            title: 'Are you sure?',
            text: 'That you want to delete *' + item + '* from your system',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then((result) => {
            if (result.value) {
            $('#deleteItem').attr('action', route);
            $('#deleteItem').submit();
        }
    });
    });
});