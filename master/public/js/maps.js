var initMap = function() {
    // Create a map object and specify the DOM element for display.
    var directionsService = new google.maps.DirectionsService();
    var directionsDisplay = new google.maps.DirectionsRenderer();

    var origin = makeCoords(document.getElementById('from_coords').innerText);
    var destination = makeCoords(document.getElementById('to_coords').innerText);


    var map = new google.maps.Map(document.getElementById('map'), {
        center: origin,
        zoom: 10
    });
    var marker = new google.maps.Marker({
        position: origin,
        map: map
    });
    var marker2 = new google.maps.Marker({
        position: destination,
        map: map
    });

    directionsDisplay.setMap(map);
    calcRoute(directionsService, directionsDisplay, origin, destination);
};

function makeCoords(latLng) {
    var coords = latLng.split(',');
    return {
        lat: +coords[0],
        lng: +coords[1]
    };
}

function calcRoute(directionsService, directionsDisplay, origin, destination) {
    var request = {
        origin: origin,
        destination: destination,
        travelMode: 'DRIVING'
    };
    directionsService.route(request, function(result, status) {
        if (status == 'OK') {
            directionsDisplay.setDirections(result);
        }
        console.log(result);
        console.log(status);
    });
}